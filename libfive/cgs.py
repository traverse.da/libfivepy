from kernel import *

# Basic operations

def union(*args):
    return minimum(*args)
Shape.__or__ = union

def intersection(*args):
    return maximum(*args)
Shape.__and__ = intersection

def difference(shape_a, *other_shapes):
    return intersection(shape_a, -union(*other_shapes))
Shape.__xor__ = difference

def inverse(shape):
    return -shape

def offset(shape, offset):
    return shape - offset

def clearance(shape_a, shape_b, clearance):
    return difference(shape_a, offset(shape_b, clearance))

def shell(shape, shell):
    return clearance(shape, shape, -shell)

def blend_expt(a, b, m):
    return -log(exp(-m*a) + exp(-m*b))/m

# 2D to 3D operations

def extrude_x(shape, xmin, xmax = 0):
    if xmax == 0 and xmin > 0: xmin, xmax = 0, xmin
    return maximum(shape, xmin-x, x-xmax)

def extrude_y(shape, ymin, ymax = 0):
    if ymax == 0 and ymin > 0: ymin, ymax = 0, ymin
    return maximum(shape, ymin-y, y-ymax)

def extrude_z(shape, zmin, zmax = 0):
    if zmax == 0 and zmin > 0: zmin, zmax = 0, zmin
    return maximum(shape, zmin-z, z-zmax)

def extrude_x_center(shape, lenght):
    return maximum(shape, -lenght/2-x, x-lenght/2)

def extrude_y_center(shape, lenght):
    return maximum(shape, -lenght/2-y, y-lenght/2)

def extrude_z_center(shape, lenght):
    return maximum(shape, -lenght/2-z, z-lenght/2)
