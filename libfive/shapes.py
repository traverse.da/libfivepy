from vector import *
from transforms import *
from cgs import *

# 2D shapes

def circle_x(radius, center = Vector(0,0,0)):
    return (y**2 + z**2 - radius**2).move(center)

def circle_y(radius, center = Vector(0,0,0)):
    return (x**2 + z**2 - radius**2).move(center)

def circle_z(radius, center = Vector(0,0,0)):
    return (x**2 + y**2 - radius**2).move(center)

def bounded_rectangle(point_a, point_b):
    return (maximum((point_a.x-x), (x-point_b.x), (point_a.y-y), (y-point_b.y)))

def rectangle(width, height = None, center = Vector(0,0,0)):
    if height == None: height = width
    return bounded_rectangle(Vector(-width/2,-height/2), Vector(width/2, height/2)).move(center)

# 3D sphapes

def sphere(radius, center = Vector(0,0,0)):
    return (x**2 + y**2 + z**2 - radius**2).move(center)

def bounded_box(point_a, point_b):
    return maximum((point_a.x-x), (x-point_b.x), (point_a.y-y), (y-point_b.y), (point_a.z-z), (z-point_b.z))

def box(width, height = None, depth = None, center = Vector(0,0,0)):
    if height == None: height = width
    if depth == None: depth = height
    return bounded_box(Vector(-width/2,-height/2, -depth/2), Vector(width/2, height/2, depth/2)).move(center)
