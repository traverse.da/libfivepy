from types import GeneratorType as generator

class Vector(list):

    def __init__(self, *args):
        if len(args) == 1:
            if isinstance(args[0], (list, tuple)): args = args[0]
            elif isinstance(args[0], (zip, generator)): args = tuple(args[0])

        for arg in args: assert isinstance(arg, (float, int))

        super().__init__(args)

    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]

    @property
    def z(self):
        return self[2]

    def __neg__(self):
        return Vector(-a for a in self)

    def __add__(self, other):
        if isinstance(other, Vector):
            assert len(other) == len(self)
            return Vector(s+o for s,o in zip(self,other))
        elif isinstance(other, (float, int)):
            return Vector(s+other for s in self)

    def __sub__(self, other):
        if isinstance(other, Vector):
            assert len(other) == len(self)
            return Vector(s-o for s,o in zip(self,other))
        elif isinstance(other, (float, int)):
            return Vector(s-other for s in self)

    def __mul__(self, other):
        if isinstance(other, (float, int)):
            return Vector(s*other for s in self)

    def __truediv__(self, other):
        if isinstance(other, (float, int)):
            return Vector(s/other for s in self)

    def __floordiv__(self, other):
        if isinstance(other, (float, int)):
            return Vector(s//other for s in self)

    def dot(self, other):
        assert len(other) == len(self)
        return sum(s*o for s,o in zip(self,other))
